FROM nginx:alpine
COPY /public/index.html /usr/share/nginx/html/
COPY /public/nginx.conf /etc/nginx/conf.d/
EXPOSE 80
